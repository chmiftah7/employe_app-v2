@extends('employee::layouts.master')

@section('content')
<section class="section">
    <div class="section-header">
      <h1>Employee</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Employee</a></div>
        <div class="breadcrumb-item">Form</div>
      </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Add Employee</h2>
        <p class="section-lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4>Create New Employee</h4>
                </div>
                <div class="card-body">
                  <form method="post" action="{{route('employee.store')}}" enctype="multipart/form-data">
                    @csrf
                      @include('employee::_form',[
                        'submit'=>'Submit'
                      ])
                  </form>
                </div>
              </div>
            </div>
        </div>
    </div>
  </section>
@endsection
