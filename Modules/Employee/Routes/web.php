<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Modules\Companie\Http\Controllers\CompanieController;
Route::group(['middleware' => ['auth']], function () {
Route::resource('employee','EmployeeController');
Route::get('dataforselect2',[CompanieController::class,'select2']);
Route::post('/import','EmployeeController@import')->name('employee.import');
});