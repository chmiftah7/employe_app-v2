<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'employee'], function () {
    Route::get('/', 'EmployeeController@getEmployee');
    Route::get('/{employee}', 'EmployeeController@getEmployeeId');
    Route::post('/', 'EmployeeController@storeEmployee');
    Route::put('/{employee}', 'EmployeeController@updateEmployee');
    Route::delete('/{employee}', 'EmployeeController@deleteEmployee');

});