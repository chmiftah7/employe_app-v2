<?php

namespace Modules\Employee\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Companie\Entities\Company;

class Employee extends Model
{
   
    protected $guarded = [];
    
    public function companies(){
        return $this->belongsTo(Company::class);
    }

        
    public function companie(){
        return $this->hasOne(Company::class);
    }
}
