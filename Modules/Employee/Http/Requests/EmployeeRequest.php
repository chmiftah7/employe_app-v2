<?php

namespace Modules\Employee\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    public function rules()
    {
        return [
            
            'nama' => 'required',
            'email'=> 'email|required',
            'companies_id'=>'required',
            'status'=>'required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
