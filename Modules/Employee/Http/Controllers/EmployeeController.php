<?php

namespace Modules\Employee\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Companie\Repositories\CompanieRepository;
use Modules\Employee\Entities\Employee;
use Modules\Employee\Http\Requests\EmployeeRequest;
use Modules\Employee\Http\Requests\ImportRequest;
use Modules\Employee\Repositories\EmployeeRepository;
use Modules\Employee\Constants\Status;
class EmployeeController extends Controller
{



    public function __construct(EmployeeRepository $employe, CompanieRepository $companie)
    {
        $this->employee = $employe;
        $this->companie = $companie;

        $this->middleware('permission:view.employee', ['only'=>'index']);
        $this->middleware('permission:create.employee', ['only'=>'create']);
        $this->middleware('permission:edit.employee', ['only'=>'edit']);
        $this->middleware('permission:delete.employee', ['only'=>'destroy']);
    }


    public function index()
    {
        $employee = $this->employee->getEmployee();
        return view('employee::index', compact('employee'));
    }


    public function create(Employee $employee)
    {
     
        $st = new Status();
        $status= $st->labels();

        $companies = $this->companie->getCompanie();
        return view('employee::create', compact('companies', 'employee','status'));
    }


    public function store(EmployeeRequest $request)
    {
   
      

        $this->employee->store($request);
        return redirect(route('employee.index'))->with('success', 'create employe succes!');
    }


    public function edit(Employee $employee)
    {
        $st = new Status();
        $status= $st->labels();
        $companies = $this->companie->getCompanie();
        return view('employee::edit', compact('employee', 'companies','status'));
    }


    public function update(EmployeeRequest $request, Employee $employee)
    {
        $this->employee->update($request, $employee);
        return redirect(route('employee.index'))->with('success', 'update employe succes!');
    }


    public function destroy(Employee $employee)
    {

        $this->employee->destroy($employee);
        return redirect(route('employee.index'))->with('success', 'delete emplye succes!');
    }

    public function import(ImportRequest $request)
    {

        $this->employee->import($request);
        return redirect(route('employee.index'))->with('success', 'import data success');
    }

    //api
    public function getEmployee()
    {
        $employee = $this->employee->getEmployee();
        return response()->json([
            'data'=>$employee,
            'status'=>'success',
            'message'=>'success'
        ]);
    }

    public function getEmployeeId(Employee $employee)
    {
        $employees = $employee->companies()->latest()->paginate(5);
        return response()->json([
            'data'=>$employees,
            'status'=>'success',
            'message'=>'success'
        ]);
    }



    public function storeEmployee(EmployeeRequest $request)
    {
        $this->employee->storeAPi($request);
        return response()->json([
            'success' => true,
            'message' => "sukses",
        ]);
    }


    public function updateEmployee(EmployeeRequest $request, Employee $employee)
    {
        $this->employee->updateAPi($request, $employee);
        return response()->json([
            'success' => true,
            'message' => "sukses",
        ]);
    }

    public function deleteEmployee(Employee $employee)
    {

        $this->employee->destroy($employee);
        return response()->json([
            'success' => true,
            'message' => "sukses",
        ]);
    }
}
