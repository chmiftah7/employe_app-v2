<?php

namespace Modules\Employee\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Employee\Entities\Employee;
use Modules\Employee\Http\Requests\EmployeeRequest;
use Modules\Employee\Http\Requests\ImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Employee\Imports\EmployeeImport;

class EmployeeRepository
{
    public function getEmployee()
    {
        return Employee::with('companies')->latest()->paginate(5);
    }
 
    public function store(EmployeeRequest $request)
    {
        Employee::create([
            'nama' => $request->nama,
            'companies_id' => $request->companies_id,
            'email' => $request->email,
            'status'=> $request->status,
        ]);
    }


    public function update(EmployeeRequest $request, Employee $employee)
    {
        $employee->update([
            'nama' => $request->nama,
            'companies_id' => $request->companies_id,
            'email' => $request->email,
        ]);
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();
    }

    public function import(ImportRequest $request)
    {
        $file = $request->file('file')->store('import');
        Excel::import(new EmployeeImport, $file);
    }


    public function  storeApi(EmployeeRequest $request)
    {
        $input = $request->all();
        Employee::create($input);
    }


    public function updateApi(EmployeeRequest $request, Employee $employee)
    {
        $input = $request->all();
        $employee->update($input);
    }
}
