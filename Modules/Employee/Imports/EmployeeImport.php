<?php

namespace Modules\Employee\Imports;


use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Modules\Employee\Entities\Employee; 

class EmployeeImport implements ToCollection, WithChunkReading, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        Validator::make($rows->toArray(), [
            '*.nama' => 'required',
            '*.email' => 'required',
            '*.companies_id' => 'required',
            '*.status' => 'required',
        ])->validate();

        foreach ($rows as $row) {
            Employee::insert([
                'nama' => $row['nama'],
                'email' => $row['email'],
                'companies_id' => $row['companies_id'],
                'status'=> $row['status'],
            ]);
        }
    }

    public function chunkSize(): int
    {
        return 500;
    }

}
