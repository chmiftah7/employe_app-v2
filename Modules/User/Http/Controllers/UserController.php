<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Routing\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Modules\User\Http\Requests\StoreUserRequest;
use Modules\User\Http\Requests\UpdateUserRequest;
use Spatie\Permission\Models\Role;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:view.user', ['only'=>'index']);
        $this->middleware('permission:create.user', ['only'=>'create']);
        $this->middleware('permission:edit.user', ['only'=>'edit']);
        $this->middleware('permission:delete.user', ['only'=>'destroy']);
    }

    public function index()
    {
        $users = User::latest()->paginate(10);
        return view('user::index', compact('users'));
    }

    public function create(User $user)
    {
        $role = Role::get();
        return view('user::create', [
            'user' => $user,
            'roles' => $role,
        ]);
    }

    public function store(User $user, StoreUserRequest $request)
    {  
        $user->syncRoles($request->get('role'));
        $user->create(array_merge($request->validated(), [
            'password' => Hash::make('password'),
        ]));
        return redirect()->route('users.index')->withSuccess(__('User created successfully.'));
    }


    public function edit(User $user)
    {
        return view('user::edit', [
            'user' => $user,
            'userRole' => $user->roles->pluck('name')->toArray(),
            'roles' => Role::latest()->get()
        ]);
    }


    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->validated());
        $user->syncRoles($request->get('role'));
        return redirect()->route('users.index')->withSuccess(__('User updated successfully.'));
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users.index')
            ->withSuccess(__('User deleted successfully.'));
    }
}
