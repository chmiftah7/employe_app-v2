@extends('permission::layouts.master')

@section('content')
@if (count($errors) > 0)
                <div class="row">
                    <div class="col-md-12 col-md-offset-1">
                      <div class="alert alert-danger alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <h4><i class="icon fa fa-ban"></i> Error!</h4>
                          @foreach($errors->all() as $error)
                          {{ $error }} <br>
                          @endforeach      
                      </div>
                    </div>
                </div>
                @endif
  
                @if (session('success'))
                    <div class="alert alert-success alert-dismissible text-center show fade">
                        <div class="alert-body">
                            <button class="close" data-dismiss="alert">
                                <span>&times;</span>
                            </button>
                            {{ session('success') }}
                        </div>
                    </div>
                @endif

<section class="section">
    <div class="section-header">
        <h1>Users</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Campaign</a></div>
            <div class="breadcrumb-item">Table</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Users Table</h2>
        <p class="section-lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        <div class="row">
            <div class="col-12">
                <x-alert />
                <div class="card">

                    <div class="card-header d-flex justify-content-between mt-2">
                        <div class="">
                            <a href="{{route('users.create')}}" class="btn btn-primary">Users<i
                                    class="fa fa-plus ml-2"></i></a>
                        </div>
                        <div class="card-header-form">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Roles</th>
                                    <th>created at</th>
                                    <th>Action</th>
                                </tr>
                                @foreach ($users as $item)
                                    <tr>

                                        <td>{{ $users->count() * ($users->currentPage() - 1) + $loop->iteration }}
                                        </td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{$item->email}}</td>
                                        <td>
                                            @foreach($item->roles as $role)
                                                <span class="badge bg-primary text-light">{{ $role->name }}</span>
                                            @endforeach
                                        </td>
                                        <td>{{ $item->created_at->format('d M Y') }}</td>
                                        <td>
                                            <div class="d-flex">
                                                <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip"
                                                    title="Edit" href="{{ route('users.edit', $item) }}"><i
                                                        class="fas fa-pencil-alt"></i></a>

                                                <form action="{{ route('users.destroy', $item) }}" method="post">
                                                    @csrf
                                                    @method('delete')

                                                    <button class="btn btn-danger btn-action" data-toggle="tooltip"
                                                        title="Delete" onclick="confirm('are you sure want delete')"><i
                                                            class="fas fa-trash"></i></button>

                                                </form>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach

                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <nav class="d-inline-block">
                            <ul class="pagination mb-0">
                                {!! $users->links() !!}
                            </ul>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
    </div>


</section>
@endsection
