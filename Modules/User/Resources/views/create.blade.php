@extends('employee::layouts.master')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Users</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Users </a></div>
                <div class="breadcrumb-item">Form</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Add Users</h2>
            <p class="section-lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Create New Users</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('users.store') }}" method="post">
                                @csrf

                                <div class="form-group">
                                    <label for="name">Nama</label>
                                    <input type="text" name="name" id="name" placeholder="name" class="form-control"
                                        value="{{ old('name') ?? $user->name }}">
                                    @error('name')
                                        <div class="text-danger mt-2">{{ $message }}</div>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label for="nama">Email</label>
                                    <input type="email" name="email" id="email" placeholder="email" class="form-control"
                                        value="{{ old('email') ?? $user->email }}">
                                    @error('email')
                                        <div class="text-danger mt-2">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="role" class="form-label">Role</label>
                                    <select class="form-control" name="role">
                                        <option value="">Select role</option>
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('role'))
                                        <span class="text-danger text-left">{{ $errors->first('role') }}</span>
                                    @endif
                                </div>
                                <button class="btn btn-primary mt-3">
                                    submit
                                </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
