@extends('permission::layouts.master')

@section('content')
@if (count($errors) > 0)
                <div class="row">
                    <div class="col-md-12 col-md-offset-1">
                      <div class="alert alert-danger alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <h4><i class="icon fa fa-ban"></i> Error!</h4>
                          @foreach($errors->all() as $error)
                          {{ $error }} <br>
                          @endforeach      
                      </div>
                    </div>
                </div>
                @endif
  
                @if (session('success'))
                    <div class="alert alert-success alert-dismissible text-center show fade">
                        <div class="alert-body">
                            <button class="close" data-dismiss="alert">
                                <span>&times;</span>
                            </button>
                            {{ session('success') }}
                        </div>
                    </div>
                @endif

<section class="section">
    <div class="section-header">
        <h1>Permission</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Permission</a></div>
            <div class="breadcrumb-item">Table</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Permission Table</h2>
        <p class="section-lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        <div class="row">
            <div class="col-12">
                <x-alert />
                <div class="card">

                    <div class="card-header d-flex justify-content-between mt-2">
                        <div class="">
                            <a href="{{route('permissions.create')}}" class="btn btn-primary">Permissions <i
                                    class="fa fa-plus ml-2"></i></a>
                        </div>
                        

                        <div class="card-header-form">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad"
                                                class="custom-control-input" id="checkbox-all">
                                            <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Guard</th>
                        
                                  
                                    <th>created at</th>
                                    <th>Action</th>
                                </tr>
                                @foreach ($permissions as $item)
                                    <tr>
                                        <td class="p-0 text-center">
                                            <div class="custom-checkbox custom-control">
                                                <input type="checkbox" data-checkboxes="mygroup"
                                                    class="custom-control-input" id="checkbox-1">
                                                <label for="checkbox-1" class="custom-control-label">&nbsp;</label>
                                            </div>
                                        </td>
                                        <td>{{ $permissions->count() * ($permissions->currentPage() - 1) + $loop->iteration }}
                                        </td>
                                        <td>{{ $item->name }}</td>
    
                                        <td>{{ $item->guard_name }}</td>
                                        <td>{{ $item->created_at->format('d M Y') }}</td>
                                        <td>
                                            <div class="d-flex">
                                                <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip"
                                                    title="Edit" href="{{ route('permissions.edit', $item) }}"><i
                                                        class="fas fa-pencil-alt"></i></a>

                                                <form action="{{ route('permissions.destroy', $item) }}" method="post">
                                                    @csrf
                                                    @method('delete')

                                                    <button class="btn btn-danger btn-action" data-toggle="tooltip"
                                                        title="Delete" onclick="confirm('are you sure want delete')"><i
                                                            class="fas fa-trash"></i></button>

                                                </form>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach

                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <nav class="d-inline-block">
                            <ul class="pagination mb-0">
                                {!! $permissions->links() !!}
                            </ul>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
    </div>


</section>
@endsection
