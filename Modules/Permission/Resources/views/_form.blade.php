<div class="form-group">
    <label for="name">Name</label>
    <input type="text" name="name" id="name" placeholder="name" class="form-control"
        value="{{ old('name') ?? $permission->name }}">
    @error('name')
        <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
</div>

<button class="btn btn-primary">
    {{$submit}}
</button>