<?php

namespace Modules\Permission\Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SeedPermissionRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'view.employee']);
        Permission::create(['name' => 'create.employee']);
        Permission::create(['name' => 'edit.employee']);
        Permission::create(['name' => 'delete.employee']);

        Permission::create(['name' => 'view.company']);
        Permission::create(['name' => 'create.company']);
        Permission::create(['name' => 'edit.company']);
        Permission::create(['name' => 'delete.company']);


        Permission::create(['name' => 'view.roles']);
        Permission::create(['name' => 'create.roles']);
        Permission::create(['name' => 'edit.roles']);
        Permission::create(['name' => 'delete.roles']);


        Permission::create(['name' => 'view.permission']);
        Permission::create(['name' => 'create.permission']);
        Permission::create(['name' => 'edit.permission']);
        Permission::create(['name' => 'delete.permission']);

        Permission::create(['name' => 'view.user']);
        Permission::create(['name' => 'create.user']);
        Permission::create(['name' => 'edit.user']);
        Permission::create(['name' => 'delete.user']);


      
        Role::create(['name' => 'staff'])
            ->givePermissionTo(['view.company', 'create.company','edit.company','delete.company', 'view.employee','create.employee','edit.employee', 'delete.employee']);
           

            $staff = User::create([
                'name' => 'Staff', 
                'email' => 'staff@transisi.id',
                'password' => Hash::make('transisi'),
            ]);
    

            $staff->assignRole('staff');

    
    }
}
