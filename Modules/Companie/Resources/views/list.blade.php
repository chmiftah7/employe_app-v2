@extends('employee::layouts.master')

@section('content')


<section class="section">
    <div class="section-header">
        <h1>Company</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Company</a></div>
            <div class="breadcrumb-item">Table</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Company {{$companie->name}}</h2>
        <p class="section-lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        <div class="row">
            <div class="col-12">
                <x-alert />
                <div class="card">

                    <div class="card-header d-flex justify-content-between mt-2">
                        <div class="">
                            <a href="{{route('employee.print', $companie)}}" class="btn btn-primary">PDF <i
                                    class="fa fa-print ml-2"></i></a>
                        </div>

                        <div class="card-header-form">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad"
                                                class="custom-control-input" id="checkbox-all">
                                            <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th>#</th>
                                    <th>name</th>
                                    <th>email</th>
                                
                                  
                                    <th>created at</th>
                                    <th>Action</th>
                                </tr>
                                @foreach ($employee as $item)
                                    <tr>
                                        <td class="p-0 text-center">
                                            <div class="custom-checkbox custom-control">
                                                <input type="checkbox" data-checkboxes="mygroup"
                                                    class="custom-control-input" id="checkbox-1">
                                                <label for="checkbox-1" class="custom-control-label">&nbsp;</label>
                                            </div>
                                        </td>
                                        <td>{{ $employee->count() * ($employee->currentPage() - 1) + $loop->iteration }}
                                        </td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->email}}</td>
                                 

              
                                        <td>{{ $item->created_at->format('d M Y') }}</td>
                                        <td>
                                            <div class="d-flex">
                                                <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip"
                                                    title="Edit" href="{{ route('employee.edit', $item) }}"><i
                                                        class="fas fa-pencil-alt"></i></a>

                                                <form action="{{ route('employee.destroy', $item) }}" method="post">
                                                    @csrf
                                                    @method('delete')

                                                    <button class="btn btn-danger btn-action" data-toggle="tooltip"
                                                        title="Delete" onclick="confirm('are you sure want delete')"><i
                                                            class="fas fa-trash"></i></button>

                                                </form>
                                                <a class="btn btn-primary btn-action ml-1" data-toggle="tooltip"
                                                title="Edit" href="{{ route('employee.show', $item) }}"><i
                                                    class="fas fa-eye"></i></a>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach

                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <nav class="d-inline-block">
                            <ul class="pagination mb-0">
                                {!! $employee->links() !!}
                            </ul>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
    </div>


</section>
@endsection
