@extends('employee::layouts.master')

@section('content')
<section class="section">
    <div class="section-header">
      <h1>Company</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Companie</a></div>
        <div class="breadcrumb-item">Form</div>
      </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Company {{$companie->name}}</h2>
        <p class="section-lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4>Create New Company</h4>
                </div>
                <div class="card-body">
                  <form method="post" action="{{route('companie.update', $companie)}}" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    @if ($companie->logo)
                    <img src="{{asset('storage/'.$companie->logo)}}" width="500"/>
                    @else 
                     <img src="https://cdn.logo.com/hotlink-ok/logo-social.png" alt="" width="500">
                    @endif
                    
                      @include('companie::_form',[
                        'submit'=>'Update'
                      ])
                  </form>
                </div>
              </div>
            </div>
        </div>
    </div>
  </section>
@endsection
