<?php

namespace Modules\Companie\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Companie\Repositories\CompanieRepository;
use Modules\Companie\Entities\Company;
use Modules\Companie\Http\Requests\CompanyRequest;
use PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class CompanieController extends Controller
{

    public function __construct(CompanieRepository $companie)
    {
        $this->companie = $companie;
    
        $this->middleware('permission:view.company', ['only'=>'index']);
        $this->middleware('permission:create.company', ['only'=>'create']);
        $this->middleware('permission:edit.company', ['only'=>'edit']);
        $this->middleware('permission:delete.company', ['only'=>'destroy']);
    }


    public function index()
    {
       

        $companie = $this->companie->getCompanie();
        return view('companie::index', compact('companie'));
    }

    public function select2(Request $request)
    {
        

        $companie = $this->companie->getSelect2Data($request);
        return $companie;
    }


    public function show(Company $companie)
    {
        $employee = $companie->employes()->latest()->paginate(5);
        return view('companie::list', compact('employee', 'companie'));
    }

    public function print(Company $companie)
    {


        //  $employee = $companie->employes()->get();
        $employee = DB::table('employees')
            ->join('companies', 'companies.id', '=', 'employees.companies_id')
            ->select('employees.nama', 'employees.email', 'employees.created_at as since', 'companies.name as companies')->where('companies_id', $companie->id)
            ->get();
        $pdf = PDF::loadview('companie::export._pdf', ['employee' => $employee, 'companie' => $companie])->setOptions(['defaultFont' => 'sans-serif']);
        return $pdf->stream('laporan-pegawai-pdf');
    }

    public function create(Company $companie)
    {
       

        return view('companie::create', compact('companie', 'companie'));
    }


    public function store(CompanyRequest $request)
    {

        $this->companie->store($request);
        return redirect(route('companie.index'))->with('success', 'create employe succes!');
    }


    public function edit(Company $companie)
    {
        $companies = $this->companie->getCompanie();
        return view('companie::edit', compact('companie', 'companies'));
    }


    public function update(CompanyRequest $request, Company $companie)
    {
        $this->companie->update($request, $companie);
        return redirect(route('companie.index'))->with('success', 'update employe succes!');
    }


    public function destroy(Company $companie)
    {

        $this->companie->destroy($companie);
        return redirect(route('companie.index'))->with('success', 'delete emplye succes!');
    }


    // api


    public function getCompany()
    {
        $companie = $this->companie->getCompanie();
        return response()->json($companie);
    }

    public function getCompanyId(Company $companie)
    {
        return response()->json($companie);
    }

    public function storeCompany(CompanyRequest $request)
    {

        Company::create([
            'name' => $request->name,
            'website' => $request->website,
            'email' => $request->email,
            'logo' => $request->file('logo')->store('app/company'),
        ]);
        return response()->json([
            'success' => true,
            'message' => "sukses",

        ]);
    }


    public function updateCompany(CompanyRequest $request, Company $companie)
    {

        if ($request->logo) {
            Storage::delete($companie->logo);

            $logo = $request->file('logo')->store('app/company');
        } else {
            $logo = $companie->logo;
        }

        $companie->update([
            'name' => $request->name,
            'website' => $request->website,
            'email' => $request->email,
            'logo' => $logo
        ]);

        return response()->json([
            'success' => true,
            'message' => "sukses",
        ]);
    }

    public function deleteCompany(Company $companie)
    {

        $companie->delete();
        return response()->json([
            'success' => true,
            'message' => "sukses",
        ]);
    }
}
