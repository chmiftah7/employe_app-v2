<?php

namespace Modules\Companie\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'website' => 'required',
            'logo' => [
                'image',
                'max:2048',
                'mimes:png',
                'dimensions:min_width=100,min_height=100',
                Rule::requiredIf(request()->routeIs('companie.store')),
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
