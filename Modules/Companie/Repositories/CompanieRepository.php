<?php


namespace Modules\Companie\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\Companie\Entities\Company;
use Modules\Companie\Http\Requests\CompanyRequest;

class CompanieRepository
{

    public function getCompanie()
    {
        return Company::latest()->paginate(5);
    }

    public function getSelect2Data(Request $request)
    {
        if ($request->ajax()) {
            
            $term = trim($request->term);
            $companie = Company::select('id','name as text')
            ->where('name', 'LIKE',  '%' . $term . '%')
            ->simplePaginate(5);
         
            $morePages = true;
            if (empty($companie->nextPageUrl())) {
                $morePages = false;
            }
            $results = array(
                "results" => $companie->items(),
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return $results;
        }
    }


    public function store(CompanyRequest $request)
    {
        Company::create([
            'name' => $request->name,
            'logo' => $request->file('logo')->store('app/company'),
            'email' => $request->email,
            'website' => $request->website,
        ]);
    }


    public function update(CompanyRequest $request, Company $companie)
    {

        if ($request->logo) {
            Storage::delete($companie->logo);

            $logo = $request->file('logo')->store('images/companie');
        } else {
            $logo = $companie->logo;
        }

        $companie->update([
            'name' => $request->name,
            'logo' => $logo,
            'email' => $request->email,
            'website' => $request->website
        ]);
    }

    public function destroy(Company $companie)
    {
        Storage::delete($companie->logo);

        $companie->delete();
    }

    public function storeApi(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'website' => 'required',
            'email' => 'required|email|unique:companies',

        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),
            ], 401);
        }
        $input = $request->all();
        Company::create($input);
    }
    public function updateApi(Request $request, Company $companie)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'website' => 'required',
            'email' => 'required',

        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),
            ], 401);
        }
        $input = $request->all();
        $companie->update($input);
    }
}
