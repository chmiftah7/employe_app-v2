<?php

namespace Modules\Companie\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Employee\Entities\Employee;

class Company extends Model
{
  

    protected $guarded = [];
    public function employes(){
        return $this->hasMany(Employee::class, 'companies_id');
    }

}
