@extends('employee::layouts.master')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Role</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Role</a></div>
                <div class="breadcrumb-item">Form</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Edit Role</h2>
            <p class="section-lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Edit Role - {{ $role->name }}</h4>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{ route('roles.update', $role) }}" enctype="multipart/form-data">
                                @csrf
                                @method('put')

                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" placeholder="name" class="form-control"
                                        value="{{ old('name') ?? $role->name }}">
                                    @error('name')
                                        <div class="text-danger mt-2">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="card">
                                        <div class="card-header">
                                            Assign Permission
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-responsive">
                                                <thead>
                                                    <th scope="col" width="1%"><input type="checkbox" name="all_permission">
                                                    </th>
                                                    <th scope="col" width="20%">Name</th>
                                                    <th scope="col" width="1%">Guard</th>
                                                </thead>
                                                @foreach ($permission as $item)

                                                    <tr>

                                                        <td>
                                                            <input type="checkbox" name="permission[{{ $item->name }}]"
                                                                value="{{ $item->name }}" class='permission'
                                                                {{ in_array($item->name, $rolePermissions) ? 'checked' : '' }}>
                                                        </td>
                                                        <td>
                                                            {{ $item->name }}
                                                        </td>
                                                        <td>
                                                            {{ $item->guard_name }}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            </table>
                                        </div>
                                    </div>


                                </div>

                                <button class="btn btn-primary">
                                    submit
                                </button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $(document).ready(function() {
            $('[name="all_permission"]').on('click', function() {

                if ($(this).is(':checked')) {
                    $.each($('.permission'), function() {
                        $(this).prop('checked', true);
                    });
                } else {
                    $.each($('.permission'), function() {
                        $(this).prop('checked', false);
                    });
                }

            });
        });
    </script>
@endsection
