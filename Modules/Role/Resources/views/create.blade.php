@extends('employee::layouts.master')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Roles</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Roles</a></div>
                <div class="breadcrumb-item">Form</div>
            </div>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="section-body">
            <h2 class="section-title">Add Roles</h2>
            <p class="section-lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            <div class="row">

                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Create New Roles</h4>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{ route('roles.store') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" placeholder="name" class="form-control"
                                        value="{{ old('name') ?? $roles->name }}">
                                    @error('name')
                                        <div class="text-danger mt-2">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="card-body">
                                    <table class="table table-responsive">
                                        <thead>
                                            <th scope="col" width="1%"><input type="checkbox" name="all_permission">
                                            </th>
                                            <th scope="col" width="20%">Permissions</th>
                                            <th scope="col" width="1%">Guard</th>
                                        </thead>
                                        @foreach ($permissions as $item)
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="permission[{{ $item->name }}]"
                                                        value="{{ old('all_permission') ?? $item->name }}"
                                                        class='permission'>
                                                </td>
                                                <td>
                                                    {{ $item->name }}
                                                </td>
                                                <td>
                                                    {{ $item->guard_name }}
                                                </td>
                                            </tr>
                                        @endforeach

                                    </table>
                                </div>


                                <button class="btn btn-primary">
                                    submit
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $(document).ready(function() {
            $('[name="all_permission"]').on('click', function() {

                if ($(this).is(':checked')) {
                    $.each($('.permission'), function() {
                        $(this).prop('checked', true);
                    });
                } else {
                    $.each($('.permission'), function() {
                        $(this).prop('checked', false);
                    });
                }

            });
        });
    </script>
@endsection
