<?php

namespace Modules\Role\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:view.roles', ['only'=>'index']);
        $this->middleware('permission:create.roles', ['only'=>'create']);
        $this->middleware('permission:edit.roles', ['only'=>'edit']);
        $this->middleware('permission:delete.roles', ['only'=>'destroy']);
    }
   
    public function index(Request $request)
    {    $roles = Role::orderBy('id', 'DESC')->paginate(5);
        return view('role::index', compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    public function create(Role $roles)
    {
        $permissions = Permission::get();
        return view('role::create', compact('permissions','roles'));
    }

    public function store(Request $request)
    {
      //  return $request;
        $request->validate( [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);

        $role = Role::create(['name' => $request->get('name')]);
        $role->syncPermissions($request->get('permission'));

        return redirect()->route('roles.index')
            ->with('success', 'Role created successfully');
    }


    public function edit(Role $role)
    {
        $role = $role;
        $rolePermissions = $role->permissions->pluck('name')->toArray();
        $permission = Permission::get();

        return view('role::edit', compact('role', 'rolePermissions', 'permission'));
    }

 
    public function update(Role $role, Request $request)
    {
        $request->validate( [
            'name' => 'required',
            'permission' => 'required',
        ]);

        $role->update($request->only('name'));

        $role->syncPermissions($request->get('permission'));

        return redirect()->route('roles.index')
            ->with('success', 'Role updated successfully');
    }


    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()->route('roles.index');
    }
}
