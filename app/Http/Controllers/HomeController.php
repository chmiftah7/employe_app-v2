<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $employee = DB::table('employees')->count();


        $companie = DB::table('companies')->count();


        $user =  DB::table('users')->count();


        // $employee_stat = DB::table('employees')->whereBetween('created_at', ['2021-12-01', '2021-12-30'])
        //     ->selectRaw('date(created_at) as date,count(*) as value')
        //     ->groupBy('date')->get();

        // return $employee_stat;

        return view('home', compact('employee', 'companie', 'user'));
    }
}
