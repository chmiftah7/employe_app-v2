
@if (session('success'))
<div class="alert alert-success alert-dismissible text-center show fade">
    <div class="alert-body">
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
      {{session('success')}}
    </div>
  </div>
@endif


@if (session('error'))
<div class="alert alert-danger alert-dismissible show fade text-center">
    <div class="alert-body">
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
      {{session('error')}}
    </div>
  </div>
@endif