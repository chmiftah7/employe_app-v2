   <div class="main-sidebar">
       <aside id="sidebar-wrapper">
           <div class="sidebar-brand">
               <a href="index.html">{{ config('app.name') }}</a>
           </div>
           <div class="sidebar-brand sidebar-brand-sm">
               <a href="index.html">{{ config('app.name') }}</a>
           </div>
           <ul class="sidebar-menu">
               <li class="menu-header">Dashboard</li>
               <li class="nav-item dropdown active">
                   <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                   <ul class="dropdown-menu">
                       <li class="active"><a class="nav-link active" href="{{ route('home') }}">Dashboard</a>
                       </li>
                   </ul>
               </li>

               <li class="menu-header">Data</li>
               <li class="nav-item dropdown">
                   <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i>
                       <span>Company</span></a>
                   <ul class="dropdown-menu">
                       <li><a class="nav-link" href="{{route('companie.index')}}">View Company</a></li>

                   </ul>
               </li>
               <li class="nav-item dropdown">
                   <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i>
                       <span>Employee</span></a>
                   <ul class="dropdown-menu">
                       <li><a class="nav-link" href="{{route('employee.index')}}">View Employee</a></li>
                   </ul>
               </li>
               <li class="menu-header">Roles and Permission</li>
               <li class="nav-item dropdown">
                   <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i>
                       <span>Roles</span></a>
                   <ul class="dropdown-menu">
                       <li><a class="nav-link" href="{{route('roles.index')}}">View Roles</a></li>

                   </ul>
               </li>
               <li class="nav-item dropdown">
                   <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i>
                       <span>Permissions</span></a>
                   <ul class="dropdown-menu">
                       <li><a class="nav-link" href="{{route('permissions.index')}}">View Permissions</a></li>
                    

                   </ul>
               </li>

               <li class="menu-header">Users</li>
               <li class="nav-item dropdown">
                   <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Users</span></a>
                   <ul class="dropdown-menu">
                       <li><a href="{{route('users.index')}}">View Users</a></li>
                   </ul>
               </li>


           </ul>

           <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
               <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
                   <i class="fas fa-rocket"></i> Documentation
               </a>
           </div>
       </aside>
   </div>
