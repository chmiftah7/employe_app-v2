<p> php artisan generate:key </p>

<p> php artisan migrate </p>

<p> php artisan db:seed </p>

<p> php artisan module:seed Permission </p>
<p> Dummy Import 9.000+ data </p> https://docs.google.com/spreadsheets/d/1PAreuVt-3NYYHD0CWqVt-b90ga8QSQxe/edit?usp=sharing&ouid=116321541889279907205&rtpof=true&sd=true

<hr/>

<h3>Login Role admin</h3>
email : admin@transisi.id<br/>
pass : transisi

<h3>login Role Staff </h3>
email : staff@transisi.id<br/>
pass : transisi

<hr/>

<h3> Endpoint Company</h3>
<p>get : /api/company </p>
<p>show : /api/company/{id} </p>
<p>post : /api/company </p>
<p>update : /api/company/{id} </p>
<p>delete : /api/company{id} </p>

<hr/>

<h3> Endpoint Employee</h3>
<p>get : /api/employee </p>
<p>show : /api/employee/{id} </p>
<p>post : /api/employee </p>
<p>update : /api/employee/{id} </p>
<p>delete : /api/employee{id} </p>
